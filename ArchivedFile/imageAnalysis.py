import dataStructures as ds
import imageReader as ir
import numpy as np
import matplotlib
import matplotlib.pyplot as plt 
#Hi it's past Julian, remember that at t = 50 (or t = 49 if you start at 0)
#that OT gets added, which is why there's a spike at fifty.

def getHighestFluroPhoto(nArrays: list)-> int:
    listOfSums = [0 for x in range (0,len(nArrays))]
    index = 0
    
    for x in nArrays:
        listOfSums[index] = np.sum(x)
        index = index + 1     

    return listOfSums.index(max(listOfSums))

def getTotalFlor(array)->int:
    return np.sum(array)

def getAverageFromList(input: list)->int:
    return sum(input)/len(input) 

def getAbsoluteAverageFlor(array)->float:
    return np.mean(buildAbsoluteArray(array))

def getAverageFlor(array)->float:
    return np.mean(array)

def buildAbsoluteArray(array)->np.array:
    array[array != 0] = 1
    return array

def getBulkMeanFlor(nArrayList: list):
    return list(map(getAverageFlor,nArrayList))

def getBulkTotalFlor(nArrayList: list):
    sumList = [n for n in range(0,len(nArrayList))]
    iter = 0
    for x in nArrayList:
        sumList[iter]= np.sum(x)
        iter = iter + 1
    
    return sumList

def displayAsHeatmap(array,title_name = "Placeholder"):
    plt.imshow(array, cmap = 'magma', interpolation = 'nearest')
    plt.title(title_name)
    plt.show()  
    plt.close()

def saveAsHeatmap(fileName: str,array,title_name = "Placeholder"):
    #print("Heatmap: attempting save")
    savedFileName = fileName + "_heatmap.png"
    plt.imshow(array, cmap = 'magma', interpolation = 'nearest')
    plt.title(title_name)
    plt.savefig(savedFileName)
    plt.close()

#this is a bad name for the funciton, because it returns a heatmap for the WHOLE experiment, not single images
def getSummationHeatmap(nArrays: list)-> np.array:
    sumArray = nArrays[0]
    firstIndex = False
    for x in nArrays:
        if firstIndex:
            sumArray = np.add(sumArray,x)
        else:
            firstIndex = True

    return sumArray


def displayLineGraph(xAxis: list, yAxis: list,name = "Default Graph", xLabel = "X-axis", yLabel = "Y-axis"):
    plt.plot(xAxis,yAxis)
    plt.title(name)
    plt.xlabel(xLabel)
    plt.ylabel(yLabel)
    plt.show()
    plt.close()


def saveLineGraph(fileName: str,xAxis: list, yAxis: list,name = "Default Graph", xLabel = "X-axis", yLabel = "Y-axis"):
    #print("Line Graph: attempting save")
    saveName = fileName + "_lineGraph"
    plt.plot(xAxis,yAxis)
    plt.title(name)
    plt.xlabel(xLabel)
    plt.ylabel(yLabel)
    plt.savefig(saveName)
    plt.close()

def exportToCSV(expName: str, input: list, headers = []):
    if " " in expName:
        expName = expName.replace(" ","_")
    fileName = expName + "_raw_data.txt"
    if headers == []:
      with open(fileName, 'w') as file:
          for expData in input:
              toWrite = formatData(expData[0],expData[1:])
              file.write(toWrite + "\n")
              
def formatData(name: str, data: list)->str:
    lineOfText = name 
    for x in data:
        lineOfText = lineOfText + "," + str(x)

    return lineOfText    


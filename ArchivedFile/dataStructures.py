class node:
    def __init__(self, dataval = None):
        self.dataval = dataval
        self.nextval = None     

class Queue: 
    def __init__(self, head):
        self.length = 1
        self.front = head 
        self.back = None 

    def getFront(self)->node:
        return self.front.dataval

    def isEmpty(self)->bool:
        return self.length < 1

    def enqueue(self, newItem:node):
        if self.front == None:
            self.front = newItem
            if self.length == 0:
                self.length = self.length + 1
        elif self.back == None:
            self.back = newItem 
            self.front.nextval = self.back 
            self.length = self.length + 1
        else:
            self.back.nextval = newItem 
            self.back = newItem

            self.length = self.length + 1 
    
    def dequeue(self):
        if Queue.isEmpty(self):
            print("Nothing to dequeue")
        else:
            toRemove = self.front 
            self.front = self.front.nextval 
            toRemove.nextval = None 
            self.length = self.length - 1
    
    def printQueue(self):
        currentNode = self.front

        while currentNode:
            print(currentNode.dataval)
            currentNode = currentNode.nextval
    
    def stringFormat(self)-> str:
        targetNode = self.front 
        toVisualize = ""

        while targetNode:
                toVisualize = toVisualize + targetNode.dataval + " -> "
                targetNode = targetNode.nextval


        return toVisualize

    def __repr__(self)->str:
        return self.stringFormat()

class Stack:
    def __init__(self):
        self.top = None 
        self.bottom = None 
        self.size = 0

    def getTop(self): 
        return self.top.dataval

    def isEmpty(self)->bool:
        return self.size == 0

    def push(self, newItem: node):
        if self.top == None:
            self.top = newItem
            self.size = self.size + 1 
        elif self.bottom == None:
            self.bottom = self.top 
            newItem.nextval = self.bottom 
            self.top = newItem 
            self.size = self.size + 1             
        else:
            newItem.nextval = self.top 
            self.top = newItem 
            self.size = self.size + 1             
    
    def pop(self):
        if not self.isEmpty():
            toRemove = self.top 
            self.top = self.top.nextval
            toRemove.nextval = None
            self.size = self.size - 1 
        else:
            print("Nothing in the stack to remove")

    def stringFormat(self)-> str:
        targetNode = self.bottom 
        toVisualize = ""

        while targetNode:
                toVisualize = toVisualize + targetNode.dataval + "\n v \n" 
                targetNode = targetNode.nextval


        return toVisualize

    def __repr__(self)->str:
        return self.stringFormat()

class bstNode:
    def __init__(self, dataval = None, leftVal = None, rightVal = None):
        self.dataval = dataval
        self.leftVal = leftVal
        self.rightVal = rightVal
    
    def is_head(self)->bool:
        return (self.leftVal == None and self.rightVal == None)
    

        
    
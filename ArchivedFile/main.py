import numpy as np
import matplotlib.pyplot as plt
import fileIO as fIO
import imageAnalysis as iA
import imageReader as iR

def filenameFormatter(fileName: str)->str:
        if ".tif" in fileName:
            fileName = fileName[:-3]
        
        if " " in fileName:
            expName = fileName.replace(" ","_")
            formattedName = expName + "_raw_data.txt"
        else:
            formattedName = fileName + "_raw_data.csv"

        return formattedName

def generateData(fileObject: fIO.fileManager,expLength: int,expName: str, fileName: str, pixelData)->list:
    #reset directory, then go to the output file
    fileName = fileName.split(".")[0]
    
    fileObject.resetDirectory()
    fileObject.changePath("output",True)
    targetDir = expName + "_Results"
    fileObject.changePath(targetDir, True)

    avgOverTime = iA.getBulkMeanFlor(pixelData)  # returns a list of averages
    florOverTime = iA.getBulkTotalFlor(pixelData)  # returns a list of floresences
    totalAverage = iA.getAverageFromList(florOverTime)  # returns an int
    summedFluoresence = iA.getSummationHeatmap(pixelData)  # returns an array
    highestFloroIndex = iA.getHighestFluroPhoto(florOverTime) #returns an int of the location that has the highest fluorescence

    if expLength > len(avgOverTime):
        axisLength = len(avgOverTime)
    else:
        axisLength = expLength

    
    
    xAxisList = [iter for iter in range(1, axisLength + 1)]


    # saves the heatmap of the total fluorescence
    totalHeatMapTitle = fileName + ": Total Instances of Fluorescence per Pixel"
    highestFluoroTitle = fileName +": Highest Fluorescence at t = " + str(highestFloroIndex + 1)  

    # saves heatmap of highest fluorescence
    arrayToLoad =  pixelData[highestFloroIndex]
    nameToSaveAs = fileName + "_Slide_" + str(highestFloroIndex)


    #heatmaps of totalFluor and highestFluor
    iA.saveAsHeatmap(
        fileName +"_TotalFluorescenceInstances", 
        summedFluoresence,
        totalHeatMapTitle)
    
    iA.saveAsHeatmap(nameToSaveAs,arrayToLoad,highestFluoroTitle)

    # average line graph
    iA.saveLineGraph(fileName + "_AverageFluorescenceOverTime",xAxisList,avgOverTime,fileName + ": Average Fluorescence Over Time","Time (s)","Number of Lit Pixels")

    # fluorsecence line graph
    iA.saveLineGraph(fileName + "_TotalFluorescenceOverTime",xAxisList,florOverTime,fileName + ": Total Fluorescence Over Time","Time (s)","Number of Lit Pixels")

    return florOverTime

def runAnalysis(startDir: str, experimentLength = 150):
    imageManager = fIO.fileManager(startDir) #object used to manage all the FileIO and related things
    
    #this loop runs through ALL folders in 'Images' directory/folder 
    for experimentName in imageManager.experimentNames:
        experimentData = [] #list used to house all of the experiment data for exporting to a csv
        print("Analyzing: ",experimentName)
        #makes a corresponding folder named 'output' 
        imageManager.makeNewFolder(imageManager.originalDir + "\\output\\" + experimentName + "_Results")  

        #moves into the directory of the experiment name then gets the filenames in the directory
        imageManager.changePath(experimentName, True)
        #fileNames = imageManager.getFileNames()
        fileNames = list(filter(iR.isTif,imageManager.getFileNames()))

        workingDir = imageManager.currentDir
        for image in fileNames:
            #returns an array if it is single image, returns a list if it is multiple images
            fileData = iR.getpixelDataToArray(image)

            if type(fileData) == list:
                newData = [image] + generateData(imageManager,experimentLength,experimentName,image,fileData)
                experimentData.append(newData)
                imageManager.goToPath(workingDir)
            else:
                imageArrayData = iR.getbulkPixelDataToArray()
                newData = [image] + generateData(imageManager,experimentLength,experimentName,experimentName,imageArrayData)
                break

        imageManager.goToPath(imageManager.previousDir)
        fileName = filenameFormatter(experimentName)

        with open(fileName, 'w') as file:
            for x in experimentData:   
                file.write(str(x).strip("[]''.\"") + "\n")
            file.close()  



        imageManager.resetDirectory()
        imageManager.changePath("Images",True)


runAnalysis("Images")

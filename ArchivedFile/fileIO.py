import os,sys

class fileManager:
    def __init__(self, startDirectory: str, debug = False) -> None:
        self.originalDir = self.setOriDir() #this is important, keep.
        self.currentDir = self.originalDir
        self.previousDir = None
        self.debug = debug 
        self.timesSwapped = 0
        self.changePath(startDirectory,True)
        self.experimentNames = os.listdir()
        self.experimentCount = len(self.experimentNames)
        
    def getFileNames(self)->list:
        return os.listdir()

    def setOriDir(self)->str:
        return os.getcwd()

    def goUp(self):
        try:
            print(f"Moving up a directory, Attempting directory swap to {self.previousDir}")
            os.chdir(self.previousDir)
        
        except:
            print("Error with supplied directory, ", sys.exc_info())
            print("\n Restoring original path")
            os.chdir(self.currentDir)
            print(f"\nCurrent directory: {os.getcwd()}\n\n")
            return 0

        else:
            print(f"Current directory: {os.getcwd()}\n\n")
            self.previousDir,self.currentDir = self.currentDir,self.previousDir
            self.timesSwapped = self.timesSwapped + 1

    def goToPath(self,targetDir: str)->bool:
        try:
            print("Moving to: ", targetDir)
            os.chdir(targetDir)
        except:
            print("Error with swap, staying in Current Directory",sys.exc_info())
            return 0
        else:
            print("Swap Successful, now in: ",os.getcwd())
            self.previousDir = self.currentDir
            self.currentDir = os.getcwd()
            self.timesSwapped = self.timesSwapped + 1
            return 1
        

    def changePath(self,newPath: str, moveDownDir = False)->bool:
        if moveDownDir:
            newDir = self.currentDir + "\\" + newPath
        else:
            newDir = self.previousDir + "\\" + newPath

        try:
            print(f"Attempting directory swap to {newDir}")
            os.chdir(newDir)
        
        except:
            print("Error with supplied directory, ", sys.exc_info())
            print("\n Restoring original path")
            os.chdir(self.currentDir)
            print(f"\nCurrent directory: {os.getcwd()}\n\n")
            return 0

        else:
            print(f"Current directory: {os.getcwd()}\n\n")
            self.previousDir = self.currentDir
            self.currentDir = newDir
            self.timesSwapped = self.timesSwapped + 1
        
        if self.debug:
            with open("a_here.txt",'w') as test:
                test.write(str(self.timesSwapped))


    def resetDirectory(self)->bool:
        try:
            print("Reseting to original directory")
            self.previousDir = None
            self.currentDir = self.originalDir
        except:
            print("Reset Failed")
            return 0
        else:
            print("Reset Successful, now in: ", self.currentDir)
            return 1

    def makeNewFolder(self, folderName: str):
        try:
            os.mkdir(folderName)
        except FileExistsError:
            print("The file already exists, cannot create folder")
        else:
            print("Folder Create")
            
    def getFiles(self)->list:
        return os.listdir()
    
    def updateExperimentNames(self):
        self.experimentNames = os.listdir()
    

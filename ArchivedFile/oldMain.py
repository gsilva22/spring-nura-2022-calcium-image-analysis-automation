import imageAnalysis as iA
import imageReader as iR
import dataStructures as dS
import fileIO as fIO

def runDefaultsOld():
    fManager = fIO.fileManager() #creates an object to manage swapping directories and whatnot
    fManager.changePath("Images",True)
    #fileDefault = "s_C001T001.tif"
    amountDefault = 150

    for file in fManager.experimentNames:
        fManager.changePath(file)
        workingFile = file + "001.tif"
        imageDataList = iR.bulkPixelDataToArray(workingFile, amountDefault)
        highestFluroValIndex = iA.getHighestFluroPhoto(imageDataList,len(imageDataList))

        picName = "s_C001T: Highest Florescence, Slide # " + str(highestFluroValIndex)
        tFloro = iA.getTotalFlorOverTime(imageDataList)
        xAxisNumbers = [x for x in range(1,len(imageDataList) + 1)]

        iA.showLineGraph(
            xAxisNumbers,
            tFloro,
            "s_C001T: Total Florescence over Time", 
            "Sample Number", 
            "Number of Lit Pixels")
        iA.saveVisual("s_C001T: Total Florescence over Time")

        iA.showData(imageDataList[highestFluroValIndex],picName)
        iA.saveVisual(imageDataList[highestFluroValIndex],picName)

        cumulativeArr = iA.getHeatmap(imageDataList)
        iA.showData(cumulativeArr,"Cumulative Array")
        iA.saveVisual(cumulativeArr,"Cumulative Array")

def runAnalysis(imageFolderLocation = "Images"):
    fManager = fIO.fileManager() #creates an object to manage swapping directories and whatnot
    fManager.changePath(imageFolderLocation)
    #fileDefault = "s_C001T001.tif"
    #amountDefault = 150

    '''
    for file in fManager.experimentNames:
        fManager.changePath(file)
    
      
        imageDataList = iR.pixelDataToArray(workingFile, amountDefault)
        highestFluroValIndex = iA.getHighestFluroPhoto(imageDataList,len(imageDataList))

        picName = "s_C001T: Highest Florescence, Slide # " + str(highestFluroValIndex)
        tFloro = iA.getTotalFlorOverTime(imageDataList)
        xAxisNumbers = [x for x in range(1,len(imageDataList) + 1)]

        iA.showLineGraph(
            xAxisNumbers,
            tFloro,
            "s_C001T: Total Florescence over Time", 
            "Sample Number", 
            "Number of Lit Pixels")

        iA.showData(imageDataList[highestFluroValIndex],picName)

        cumulativeArr = iA.getHeatmap(imageDataList)
        iA.showData(cumulativeArr,"Cumulative Array")
    '''
    
def runDefaults():
    fManager = fIO.fileManager()
    fManager.changePath("Images")
    directoryListings = fIO.getFiles()

    for directory in directoryListings():
        if directory == directoryListings[0]:
            fManager.changePath(directory, True)
        else:
            fManager.changePath(directory, False)
    
        #Gets all the image information in a list
        imageDataList = iR.getbulkPixelDataToArray()
        
        #Gets the location of the image with the highest florescence
        highestFluroValIndex = iA.getHighestFluroPhoto(imageDataList)
       
        #Gets the mean florescence of each index
        meanFluroList = iA.getMeanFlorOverTime()
        


runAnalysis()


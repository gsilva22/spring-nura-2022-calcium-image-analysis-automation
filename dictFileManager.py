import os,sys,string

class fileManager:
    def __init__(self, startDirectory: str, expCount: int,isBulk = True,) -> None:
        #default, does all of the analysis within a given folder
        #note that this only works on folders that have subfolders
        #it will produce an error otherwise   
        if isBulk:
            self.originalDirectory = os.getcwd()
            self.fileData = getFilesDict(startDirectory)
            self.expCount = expCount
        #this is for doing analysis on a specifc folder
        #this assumes that there is not any subfolders.
        #if you have a folder with a lot of subfolders that you just want to do analysis on,
        #use either the bulk functionality or flatten the file structure
        else:
            #sets the orignal directory as the directory you're currently in
            self.originalDirectory = startDirectory
            
            #attempts to set to the designated directory
            status = self.goToPath(startDirectory)

            #failed to set to the designated directory
            if status == 0:
                print("Issue with given file path. Please try again with a valid file path or name")
                exit()

            #on sucess, it continues to get all the data within the folder
            self.fileData = self.getFileNames(startDirectory)
            self.expCount = expCount
    
    def replaceSpaces(self,files: str):
        for x in range(0,len(files)):
            if " " in files[x]:
                files[x] = files[x].replace(" ","_")

    def getCurrentDir(self)->str:
        print(f'You are currently in {os.getcwd()}')
        return os.getcwd()

    def getFileName(self,filePath: str)->str:
        return os.path.basename(filePath)

    def makeNewPath(self,pathOne: str, pathTwo: str):
        return os.path.join(pathOne,pathTwo)
    
    def makeNewFolder(self,folderPath: str)->bool:
        try:
            #im like 80% sure that you have to name the EXACT path. 
            #so do that <3
            os.mkdir(folderPath)
        except FileExistsError:
            print("The file already exists, cannot create folder")
            return 0
        else:
            print("Folder Create: ",folderPath)       
            return 1
        
    def getFileNames(self,directory = "")->list:
        if directory == "":
            files = os.listdir()
        else:
            files = os.listdir(directory)
        
        return files

    def goToPath(self,targetDir: str)->bool:
        try:
            #print("Moving to: ", targetDir)
            os.chdir(targetDir)
        except:
            print("Error with swap, staying in Current Directory",sys.exc_info())
            return 0
        else:
            #print("Swap Successful, now in: ",os.getcwd())
            return 1
        
    def resetDirectory(self)->bool:
        
        print("Reseting to original directory")
        success = self.goToPath(self.originalDirectory)

        if success == 0:
            print("Reset Failed")
            return 0
        else:
            print("Reset Successful, now in: ", self.currentDir)
            return 1

    def displayDictionary(self):
        for entry in self.fileData:
            print(f'For the value {entry}, here are the associated values:')
            for x in self.fileData.get(entry):
                print(x)
            print("\n")


class IOManager(fileManager):
    def __init__(self):
        self.startDir = os.getcwd()
        

def currentDir()->str:
    return os.getcwd()

def getFilesDict(directory: str,contents = [], parent = None):
        #gets the files in the directory (cont is NOT an iterable I dont care what you think)
        with os.scandir(directory) as cont:
            folders = {}
            
            #I know I said it wasn't an iterable but we're going to iterate through cont
            #but I promise you it's not an iterable it's weird
            #it's a specific scandir obj
            for file in cont:
                #gets the absolute path
                path = os.path.abspath(file)
                
                #only does things if the item is a directory/folder
                #the "not '.' in file.name" portion filters out hidden folders
                if not file.is_file() and not "." in file.name:

                    #sets the parent for the dictionary key
                    if parent == None:
                        parent = file.name
                        #print(f'Parent set to {parent}')
                    
                    #if the current file is NOT on the same branch of the file in question, 
                    #pass the data into the dictionary and reset the contents and parent variable 
                    elif parent not in path:
                        #print(f'{parent} is not in {path}')
                        folders.update({parent: contents})
                        contents = []
                        parent = file.name   

                    contents.append(path)
                    getFilesDict(path,contents=contents,parent = parent)

        folders.update({parent: contents})

        return folders

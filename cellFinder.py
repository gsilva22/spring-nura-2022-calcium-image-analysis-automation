import numpy as np
from scipy import stats
import sys,os
import imageAnalysis as iA

class Cell:
    def __init__(self,name: str,setOfPositions = set(),shape = (512,512), ROIbounds = [], cellSize = -1):
        self.locations = setOfPositions
        if cellSize == -1:
            self.cellSize = len(setOfPositions)
        else:
            self.cellSize = int(cellSize)
        
        self.ROIpositions = self.getBounds(ROIbounds)
        self.display = np.zeros(shape)
        self.name = str(name)
        self.shape = tuple(shape)
    
    def __repr__(self) -> str:
        return f'Cell #{self.name}| Size: {str(self.cellSize)}| Shape: {str(self.shape)}'

    def getCellSize(self)->int:
        return self.cellSize

    def showLocations(self):
        for x in self.locations:
            print(x)
    
    def doesContain(self,location: tuple)->bool:
        return location in self.locations
    
    def getBounds(self,importedBounds = [])->list:
        if importedBounds == []:
            xMin,xMax,yMin,yMax = None,None,None,None

            for loc in self.locations:
                if xMin == None:
                    xMin = loc[0]
                elif loc[0] < xMin:
                    xMin = loc[0]
                
                if xMax == None:
                    xMax = loc[0]
                elif loc[0] > xMax:
                    xMax = loc[0]
                
                if yMin == None:
                    yMin = loc[1]
                elif loc[1] < yMin:
                    yMin = loc[1]
                
                if yMax == None:
                    yMax = loc[1]
                elif loc[1] > yMax:
                    yMax = loc[1]
        else:
            xMin,xMax,yMin,yMax = importedBounds[0],importedBounds[1],importedBounds[2],importedBounds[3]
        
        self.ROIpositions = [(xMin,yMin),(xMin,yMax),(xMax,yMin),(xMax,yMax)]

        return [xMin,xMax,yMin,yMax]

    def showCell(self):
        for pos in self.locations:
            self.display[pos[0]][pos[1]] = 1
        
        iA.displayAsHeatmap(self.display,self.name)
   
class cellContainer(Cell):
    def __init__(self,listOfCells:list):
        self.Cells = listOfCells
        self.OutlierCells = self.cleanData()
        self.displayCells()
        print(f'found {len(self.OutlierCells)} cells')

    def addCell(self,newCell: Cell):
        self.Cells.append(newCell)

    def addCells(self,newCellList: list):
        self.Cells = self.Cells + newCellList

    def cellCount(self):
        return len(self.Cells)

    def displayCells(self):
        for x in self.OutlierCells:
            x.showCell()

    def cleanData(self,fName = "Placeholder")->list:
        upperBound = int(.95*len(self.Cells))
        mergeSort(self.Cells)

        return self.Cells[upperBound:]
    
    @staticmethod
    def exportCellData(expName: str, cellData: list):
        for cell in cellData:
            if not isinstance(cell, Cell):
                raise TypeError("Object is not of type 'Cell")

            cellName = expName + "-" + cell.name
            fileName = cellName + ".cell"

            data = [
                str(cell.name) + "\n",
                str(cell.cellSize) + "\n",
                str(cell.shape) + "\n",
                str(cell.locations) + "\n",
                str(cell.ROIpositions) + "\n"
            ]

            with open(fileName, "w") as fileOutput:
                fileOutput.writelines(data)

    @staticmethod
    def importCellData(fileName: str) -> Cell:
        with open(fileName, "r") as fileContents:
            fileData = fileContents.readlines()

        # Extract data from the file
        cellName = fileData[0].strip()  # Strip newline character
        cellSize = int(fileData[1].strip())
        shape = fileData[2].strip()
        locations = fileData[3].strip()
        ROIpositions = fileData[4].strip()

        formattedItems = cellContainer.formatData([shape,locations,ROIpositions])
        
        items = [cellName,cellSize] + formattedItems


        importedCell = Cell(items[0], items[3],items[2],items[-1])

        return importedCell
    
    @staticmethod
    def formatData(importedData: list)->list:
        shape,Loc,ROIs = importedData[0],importedData[1],importedData[2]
        shape = shape.strip("()").split(",")
        Loc = Loc.strip("{}").split(",")
        ROIs = ROIs.strip("[]").split(",")
        formattedLoc = []
        formattedROI = [int(index) for index in ROIs]

        for index in range(0,len(Loc) - 1,2):
            pos1 = int(list(filter(str.isnumeric,Loc[index]))[0])
            pos2 = int(list(filter(str.isnumeric,Loc[index + 1]))[0])

            formattedLoc.append((pos1,pos2))
        
        formattedShape = (int(shape[0]),int(shape[1]))

        return [formattedShape,formattedLoc,formattedROI]


def mergeSort(arr: list):
    if len(arr) > 1:
        # Create sub_array2 ← A[start..mid] and sub_array2 ← A[mid+1..end]
        mid = len(arr)//2
        sub_array1 = arr[:mid]
        sub_array2 = arr[mid:]

        # Sort the two halves
        mergeSort(sub_array1)
        mergeSort(sub_array2)
        
        # Initial values for pointers that we use to keep track of where we are in each array
        i = j = k = 0

    # Until we reach the end of either start or end, pick larger among
    # elements start and end and place them in the correct position in the sorted array
        while i < len(sub_array1) and j < len(sub_array2):
            if sub_array1[i].cellSize < sub_array2[j].cellSize:
                arr[k] = sub_array1[i]
                i += 1
            else:
                arr[k] = sub_array2[j]
                j += 1
            k += 1

    # When all elements are traversed in either arr1 or arr2,
    # pick up the remaining elements and put in sorted array
        while i < len(sub_array1):
            arr[k] = sub_array1[i]
            i += 1
            k += 1

        while j < len(sub_array2):
            arr[k] = sub_array2[j]
            j += 1
            k += 1

def searchMatrix(arrayData: np.array):
    #iA.displayAsHeatmap(arrayData)
    
    arrayData = normalizeData(arrayData)
    
    borders = arrayData.shape 
    cellList = []
    visited = set()
    cellName = 0

    for x in range(0,borders[0]):
        for y in range(0,borders[1]):
            if arrayData[x][y] == 1 and (x,y) not in visited:
                posSet = findNeighbors((x,y),arrayData)
                newCell = Cell(cellName,posSet[1],arrayData.shape)
                cellName = cellName + 1
                
                if newCell.cellSize > 0:
                    cellList.append(newCell)
                    print(cellList)
                    
                visited.update(posSet[0]),visited.update(posSet[1])
                posSet[1] = set()


    return cellList

def findNeighbors(location: tuple,matrix: np.array,visited = [set(),set()], cont = True)->tuple:
    xLocation = location[0]
    yLocation = location[1]

    #important note:
    #visited[0]: houses all locations visited
    #visited[1]: houses all locations adjacent to each other

    newLocations = []

    validNeighbors = getNeighbors((xLocation,yLocation), matrix.shape)

    for loc in validNeighbors:
        if loc not in visited[0]:
            visited[0].add(loc)
            
            if matrix[loc[0]][loc[1]] == 1:
                try:
                    visited[1].add(loc)
                    visited = findNeighbors(loc,matrix,visited)
                except RecursionError:
                    return visited
        else:
            continue

    return visited

def isValid(position: tuple, bounds: tuple):
    xValid = False
    yValid = False

    if position[0] >= 0 and position[0] < bounds[0]:
        xValid = True 
    
    if position[1] >= 0 and position[1] < bounds[1]:
        yValid = True 
    
    if xValid == True and yValid == True:
        return True 
    else:
        return False 

def getNeighbors(position: tuple, bounds: tuple)->list:
    possibleNeighbors = [(-1,-1),(0,-1),(1,-1), (-1,0),(1,0), (1,1),(0,1),(1,1)]   
    validList = []

    for pLoc in possibleNeighbors:
        xLoc = position[0] +pLoc[0]
        yLoc = position[1] +pLoc[1] 
        
        if isValid((xLoc,yLoc), bounds):
            validList.append((xLoc,yLoc))
    

    return validList

def normalizeData(arrayData: np.array)->np.array:
    highestVal = np.max(arrayData)
    arrayData = arrayData/highestVal
    return arrayData


#put the cells in a list then only take the cells with the highest amount (25%)
'''
testMatrix = [ [1,1,1,1,1,1,1,0,0,0,0,0], [1,1,1,1,0,0,0,0,1,1,0,0], [0,1,1,1,1,0,0,0,1,1,0,0], [0,0,0,1,1,0,0,0,0,0,0,0], 
              [0,0,0,1,1,0,0,0,0,0,0,0], [0,0,0,0,0,0,0,0,0,0,0,0], [0,0,0,0,0,0,1,1,1,1,1,1], [0,0,0,0,0,0,1,1,1,1,1,1],
              [0,0,0,0,0,0,1,1,1,1,1,1], [0,1,1,1,0,0,0,1,1,1,1,1], [1,1,1,1,1,0,0,0,1,1,1,0], [0,1,1,1,0,0,0,0,0,1,1,1],
              [0,0,0,0,0,0,0,0,0,0,1,1], [0,0,0,0,0,0,0,0,0,0,0,1]
             ]

testList =[(0,2),(11,3),(4,5),(2,15),(8,9),(6,4)]             

array = np.array(testMatrix)
data = searchMatrix(array)
fileName = "TestExp"
startDir = os.getcwd()
filePath = os.path.join(startDir,fileName)

cellContainer.exportCellData(fileName,data)

files = os.listdir()
cellFiles = [x for x in files if ".cell" in x]
cellList = []

newCell = cellContainer.importCellData(cellFiles[0])
print(newCell,type(newCell))
'''
